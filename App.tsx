import { NativeRouter, Routes, Route } from "react-router-native";
import MainMenu from "./src/screens/mainMenu/MainMenu";
import { Provider } from "react-redux";
import store from "./src/data/store";
import { StatusBar } from "react-native";
import WaitRoom from "./src/screens/WaitRoom/WaitRoom";

 
export default function App() {
    return (
        <Provider store={store}>
            <NativeRouter>
                <Routes>
                    <Route path="/" element={<MainMenu/>} />
                    <Route path="/main-menu" element={<MainMenu />} />
                    <Route path="/wait-room" element={<WaitRoom />} />
                </Routes>
            </NativeRouter>
            
            <StatusBar
                animated={true}
                backgroundColor="#000000"
                hidden={false}
            />
        </Provider>
    );
}
