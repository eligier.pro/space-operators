import styled from "styled-components/native";
import { TouchableOpacity, Text, FlatList, View } from "react-native";

export const StyledButton = styled(TouchableOpacity)`
  background-color: #3c8bdfa4;
  margin: 10px;
  padding: 10px 50px;
  border-radius: 5px;
  align-items: center;
  border-color: #00d9ff;
  border-style: solid;
  border-width: 1px;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 22px;
`;

export const StyledText = styled(Text)`
  color: #ffffff;
  text-align: center;
  font-size: 22px;
`

export const StyledTitle = styled(Text)`
  color: #ffffff;
  text-align: center;
  font-size: 30px;
  font-weight: 600;
`

export const StyledTextField = styled(Text)`
  color: #ffffff;
  text-align: center;
  font-size: 22px;
  background-color: #dcffff5e;
  margin: 10px;
  padding: 10px 20px;
  border-radius: 5px;
  align-items: center;
  border-style: solid;
  border-width: 1px;
`

export const StyledViewPlayer = styled(View)`
  background-color: #dcffff5e;
  text-align: center;
  margin: 10px;
  padding: 10px 50px;
  border-radius: 5px;
  align-items: center;
  border-style: solid;
  border-width: 1px;
`

export const ViewContainer = styled(View)`
  background-color: #3c8bdfa4;
  border-color: #00d9ff;
  text-align: center;
  margin: 10px;
  padding: 10px 20px;
  border-radius: 5px;
  align-items: center;
  border-style: solid;
  border-width: 1px;
  height: 200px;
`

export const ViewTitle = styled(View)`
  background-color: #3c8bdfa4;
  border-color: #00d9ff;
  text-align: center;
  margin: 10px;
  padding: 10px 20px;
  border-radius: 5px;
  align-items: center;
  border-style: solid;
  border-width: 1px;
`