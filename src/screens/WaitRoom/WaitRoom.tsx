import React, { useEffect } from 'react';
import { FlatList, ImageBackground, View } from 'react-native';
import { useNavigate } from 'react-router-native';
import { MenuStyle } from '../mainMenu/MainMenu.style';
import { ButtonText, StyledButton, StyledText, StyledTitle, StyledViewPlayer, ViewContainer, ViewTitle } from './WaitRoom.style';
import { useDispatch, useSelector } from 'react-redux';
import useWebSocket from '../../hooks/WebSocket';
import { RootState } from '../../data/store';
import RandomUsername from '../../data/RandomUsername';
import { setStatus, setUsername } from '../../data/PlayerSlice';
import useApi from '../../hooks/Api';


const WaitRoom = () => {
    const { connect, disconnect, send, players } = useWebSocket();
    const { setReady } = useApi();
    const navigate = useNavigate();
    const player = useSelector((state: RootState) => state.player);
    const gameId = useSelector((state: RootState) => state.gameId);
    const dispatch = useDispatch();

    useEffect(() => {
        if (gameId && player) {
            if(!player.name) {
                dispatch(setUsername({name: RandomUsername()}))
            } else {
                connect(() => {
                    send({
                        gameId: gameId,
                        player: {
                            uuid: player.uuid,
                            name: player.name
                        }
                    });
                });
            }
        }
    }, [gameId, player, connect, send]);

    const handleBack = () => {
        dispatch(setStatus(false));
        disconnect();
        navigate('/main-menu');
    };
    
    const handleReady = () => {
        setReady();
    }

    return (
        <ImageBackground source={require('../../../assets/images/before-create-bg.png')}>
            <MenuStyle>
                <View>                    
                    {gameId ? (
                        <>
                            <ViewTitle>
                                <StyledTitle>New game !</StyledTitle>
                                <StyledText>Game Id : {gameId}</StyledText>
                            </ViewTitle>
                            
                            <ViewContainer>
                                <StyledText>Players :</StyledText>

                                <FlatList
                                    data={players}
                                    renderItem={({ item }) =>
                                        <StyledViewPlayer>
                                            <StyledText>
                                                {item.name || 'N/A'} | {item.status ? 'Ready' : 'Not ready'}
                                            </StyledText>
                                        </StyledViewPlayer>}
                                    keyExtractor={(item, index) => `player-${item.name}-${index}`}
                                />
                            </ViewContainer>

                            <StyledButton onPress={handleReady}>
                                <ButtonText>{player.status ? 'Waiting' : 'Ready'}</ButtonText>
                            </StyledButton>

                            <StyledButton>
                                <ButtonText>Start</ButtonText>
                            </StyledButton>
                        </>
                    ) : (
                        <StyledText>Loading game ...</StyledText>
                    )}
                    
                    <StyledButton onPress={handleBack}>
                        <ButtonText>Back</ButtonText>
                    </StyledButton>
                </View>
            </MenuStyle>
        </ImageBackground>
    );
};

export default WaitRoom;