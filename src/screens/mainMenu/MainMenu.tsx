import React, { useEffect } from 'react';
import { BackHandler, View } from 'react-native';
import 'react-native-get-random-values';
import { useNavigate } from 'react-router-native';
import JoinPopup from '../JoinPopup/JoinPopup';
import RandomUsername from '../../data/RandomUsername';
import { ImageBackground, Image } from 'react-native';
import { setUsername } from '../../data/PlayerSlice';
import { useDispatch, useSelector } from 'react-redux';
import { MenuStyle, StyledButton, ButtonText, StyledText } from './MainMenu.style';
import { StyledTextInput } from '../JoinPopup/JoinPopup.style';
import { Player } from '../../models/Player';
import useApi from '../../hooks/Api';

const MainMenu = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const player = useSelector((state: {player: Player}) => state.player);
    const { processGameId } = useApi();

    const handleSetUsername = (name: any) => {
        dispatch(setUsername({name})); 
    };

    const handleRandomizeUsername = () => {
        dispatch(setUsername({name: RandomUsername()}));
    };

    const handleCreateGame = () => {
        processGameId();
        navigate('/wait-room');
    };
    
    return (
        <ImageBackground source={require('../../../assets/images/menu-bg.png')}>
            <MenuStyle>
                <View>
                    <Image source={require('../../../assets/images/so.png')} style={{width: 300, height: 132}}/>
                    <StyledTextInput
                        onChangeText={handleSetUsername}
                        value={player.name}
                        placeholder='Username'
                    />
                    <StyledButton onPress={handleRandomizeUsername}>
                        <ButtonText>Randomize</ButtonText>
                    </StyledButton>

                    <StyledText>{player.uuid}</StyledText>

                    <StyledButton onPress={handleCreateGame}>
                        <ButtonText>Create game</ButtonText>
                    </StyledButton>

                        
                    <JoinPopup/>

                    <StyledButton>
                        <ButtonText>History</ButtonText>
                    </StyledButton>

                    <StyledButton onPress={() => BackHandler.exitApp()}>
                        <ButtonText>Leave</ButtonText>
                    </StyledButton>
                </View>
            </MenuStyle>
        </ImageBackground>
    );
};

export default MainMenu;