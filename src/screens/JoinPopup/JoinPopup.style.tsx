import styled from 'styled-components/native';
import { TouchableOpacity, TextInput, Text } from 'react-native';

export const StyledButton = styled(TouchableOpacity)`
  background-color: #3c8bdfa4;
  margin: 10px;
  padding: 5px 10px;
  border-radius: 5px;
  align-items: center;
  border-color: #00d9ff;
  border-style: solid;
  border-width: 1px;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 22px;
`;

export const BgView = styled.View`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 30px;
  justify-content: center;
  background-color: #000000ca;
`;


export const StyledView = styled.View`
    background-color: #3c8bdfa4;
    padding: 30px;
    justify-content: center;
    border-color: #00d9ff;
    border-style: solid;
    border-width: 1px;
    border-radius: 10px;
`;

export const StyledTextInput = styled(TextInput)`
    height: 40px;
    margin: 10px;
    border-radius: 5px;
    color: #ffffff;
    font-size: 18px;
    padding-left: 10px;
    background-color: #dcffff5e;
`;

export const StyledText = styled(Text)`
  color: #ffffff;
  text-align: center;
  font-size: 26px;
  font-weight: 400;
`