import React, { useState } from 'react';
import { StyledButton, ButtonText, StyledView, StyledTextInput, BgView, StyledText} from './JoinPopup.style';
import { Modal } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../data/store';
import { useNavigate } from 'react-router-native';
import { setUsername } from '../../data/PlayerSlice';
import { setGameId } from '../../data/GameIdSlice';


const JoinPopup = () => {
    const [modalVisible, setModalVisible] = useState(false);
    const player = useSelector((state: RootState) => state.player);
    const gameId = useSelector((state: RootState) => state.gameId);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleJoinGame = () => {
        navigate('/wait-room');
    };

    const handleSetUsername = (name: any) => {
        dispatch(setUsername({name}));
    };

    
 return (
    <>
        <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(false);
            }}
        >
            
            <BgView>
            <StyledView>
                <StyledText>User Name :</StyledText>
                <StyledTextInput
                        onChangeText={handleSetUsername}
                        value={player.name}
                        placeholder='Username'
                    />
                <StyledText>Game ID :</StyledText>
                <StyledTextInput
                    onChangeText={gameId => dispatch(setGameId(gameId))}
                    value={gameId}
                    placeholder='Game Id'
                />

                <StyledButton onPress={handleJoinGame}>
                    <ButtonText>Validate</ButtonText>
                </StyledButton>
                <StyledButton onPress={() => setModalVisible(false)}>
                    <ButtonText>Close</ButtonText>
                </StyledButton>
            </StyledView>
            </BgView>
        </Modal>
        
        <StyledButton onPress={() => setModalVisible(true)}>
            <ButtonText>Join game</ButtonText>
        </StyledButton>
    </>
 );
};

export default JoinPopup;
