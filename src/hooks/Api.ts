import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { RootState } from '../data/store';
import { setGameId } from '../data/GameIdSlice';
import { setStatus } from '../data/PlayerSlice';

function useApi() {
    const urls = useSelector((state: RootState) => state.urls);
    const gameId = useSelector((state: RootState) => state.gameId);
    const player = useSelector((state: RootState) => state.player);
    const dispatch = useDispatch();


    const processGameId = async () => {
        if (!gameId) {
            await axios.post(urls.apiRoot + 'create-game')
            .then((response) => {
                dispatch(setGameId(response.data.id));
            })
            .catch((error) => console.error('Error getting game id:', error));
        }
    };

    const setReady = async () => {
        await axios.post(urls.apiRoot + 'ready/' + player.uuid)
        .then(() => {
            dispatch(setStatus(true));
        })
        .catch((error) => console.error('Error setting ready :', error));
    }

    return { processGameId, setReady };
}

export default useApi;