import { useRef, useState, useCallback } from 'react';
import { Urls } from '../models/Urls';
import { useSelector } from 'react-redux';
import { Player } from '../models/Player';

function useWebSocket() {
    const urls = useSelector((state: Urls) => state.urls);
    const [players, setPlayers] = useState<Player[]>([]);
    const [wsStatus, setWsStatus] = useState('disconnected');
    const ws = useRef<WebSocket | null>(null);

    const connect = useCallback((onOpenCallback: () => void) => {
        if (ws.current) {
            console.log("WebSocket already open.");
            return;
        }

        console.log("Initializing WebSocket connection...");
        ws.current = new WebSocket(urls.wsRoot);
        setWsStatus('connecting');

        ws.current.onopen = () => {
            console.log("WebSocket connection established");
            setWsStatus('connected');
            if (onOpenCallback) {
                onOpenCallback();
            }
        };

        ws.current.onmessage = (event) => {
            console.log("Received data:", event.data);
            if (event.data.startsWith('{')) {
                try {
                    const data = JSON.parse(event.data);
                    if (data.type === "players") {
                        setPlayers(data.data.players);
                    }
                } catch (e) {
                    console.error("Error parsing data:", e);
                }
            } else {
                console.log("Non-JSON data received:", event.data);
            }
        };

        ws.current.onerror = (error) => {
            console.error("WebSocket error:", error);
        };

        ws.current.onclose = () => {
            console.log("WebSocket connection closed");
            setWsStatus('closed');
        };
    }, []);

    const disconnect = useCallback(() => {
        if (!ws.current) {
            console.log("WebSocket not open.");
            return;
        }
        console.log("Closing WebSocket connection...");
        ws.current.close();
        ws.current = null;
        setWsStatus('disconnected');
    }, []);

    // Method to send data through the WebSocket
    const send = useCallback((data: { gameId: any; player: { uuid: any; name: any; }; }) => {
        if (ws.current && ws.current.readyState === WebSocket.OPEN) {
            ws.current.send(JSON.stringify({
                "type": "connect",
                "data": {
                    "gameId": data.gameId,
                    "playerId": data.player.uuid,
                    "playerName": data.player.name
                }
            }));
        }
    }, []);

    return { connect, disconnect, send, players };
}


export default useWebSocket;