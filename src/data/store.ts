import { configureStore } from '@reduxjs/toolkit';
import playerReducer from './PlayerSlice';
import urlsReducer from './UrlsSlice';
import gameIdReducer from './GameIdSlice';

const store = configureStore({
    reducer: {
      urls: urlsReducer,
      player: playerReducer,
      gameId: gameIdReducer,
    },
});
  
// Define RootState type
export type RootState = ReturnType<typeof store.getState>;
 
export default store;