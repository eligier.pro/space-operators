const RandomUsername = () => {
    const names = [
        'Estelle',
        'Marie',
        'Alexandre',
        'Laurent',
        'Philippe',
        'Andrew',
        'Romain',
        'Alexis',
        'Mehdi',
        'Aleksandra',
        'Céline',
        'William',
    ];

    const randInt = Math.floor(Math.random() * names.length)

    return(names[randInt]);
}

export default RandomUsername;