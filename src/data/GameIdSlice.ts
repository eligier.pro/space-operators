import { PayloadAction, createSlice } from '@reduxjs/toolkit';

const gameIdSlice = createSlice({
   name: 'gameId',
   initialState: '',
   reducers: {
      setGameId: (state, action) => {
         return action.payload;
      },
   },
});

export const { setGameId } = gameIdSlice.actions;

export default gameIdSlice.reducer;