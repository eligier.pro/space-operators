import { createSlice } from '@reduxjs/toolkit';

const root = '://space-operators-bb2423167918.herokuapp.com/'

const urlsSlice = createSlice({
   name: 'urls',
   initialState: {
      apiRoot: 'https' + root,
      wsRoot: 'ws' + root
   },
   reducers: {}, 
});

export default urlsSlice.reducer;