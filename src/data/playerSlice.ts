import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

const playerSlice = createSlice({
   name: 'player',
   initialState: {
      uuid: uuidv4(),
      name: '',
      status: false
   },
   reducers: {
      setUsername: (state, action) => {
         state.name = action.payload.name; 
      },
      setStatus: (state, action) => {
         state.status = action.payload;
      }
   },
});

export const { setUsername, setStatus } = playerSlice.actions;

export default playerSlice.reducer;