export interface Urls {
    urls: {
        apiRoot: string;
        wsRoot: string;
    }
}
  
export const Urls = (
    { apiRoot, wsRoot }: {
        apiRoot: string,
        wsRoot: string
    }
): Urls => ({
    urls: {
        apiRoot,
        wsRoot
    }
});