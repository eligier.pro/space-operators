export interface GameId {
    gameId: string;
}
  
export const GameId = (
    gameId: string
): GameId => ({
    gameId
});