export interface Player {
    uuid: string;
    name: string;
    status: boolean;
  }
  
  export const Player = (
    uuid: string,
    name: string,
    status: boolean
  ): Player => ({
    uuid,
    name,
    status
  });